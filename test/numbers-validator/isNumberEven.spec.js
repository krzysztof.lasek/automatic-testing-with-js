// Importing the 'expect' function from the 'chai' library to perform assertions
// const { expect } = require('chai');
import { expect } from 'chai'

// Importing functions 'describe', 'beforeEach', 'afterEach' and 'it' from 'mocha' which is a test framework.
// const { describe, beforeEach, afterEach, it } = require('mocha');
import { describe, beforeEach, afterEach, it } from 'mocha'

// Importing the NumbersValidator class from the relative path '../app/numbers-validator'
// so we can test its methods.
// const NumbersValidator = require('../app/numbers-validator');

import { NumbersValidator } from '../../app/numbers_validator.js'

// 'describe' is used to group related tests together into a test suite.
// Here, it describes a suite of tests for the 'isNumberEven' method of NumbersValidator class.
describe('isNumberEven', function () {
  // Declaring a variable 'validator' outside of the 'beforeEach' and 'it' blocks
  // to make it accessible throughout this describe block.
  let validator;

  // 'beforeEach' is a hook that runs before each test ('it' block) within this 'describe' block.
  // It's usually used for setting up the test environment.
  beforeEach(function () {
    // Instantiates a new NumbersValidator object before each test and assigns it to 'validator'
    validator = new NumbersValidator();
  })

  // 'afterEach' is a hook that runs after each test. It is often used for cleanup activities.
  afterEach(function () {
    // Sets the validator variable to null to clean up memory after each test
    validator = null;
  })

  // 'it' is used for individual test cases - it includes the actual test.
  // The string argument describes what the test should do.
  it('should return true if number is even', function () {
    // Using 'expect' to assert that the 'isNumberEven' method returns true when
    // passed the even number 4. The '.to.be.equal(true)' is the actual assertion check.
    expect(validator.isNumberEven(4)).to.be.equal(true);
  });

  it('should throw an error if number is a string', function () {
    expect(() => {
      validator.isNumberEven('4');
    }).to.throw('[4] is not of type "Number" it is of type "string"');
  });

  // Additional tests would follow for different test cases, such as testing if an odd number
  // returns false or if passing a non-number throws an error.
})

describe('getEvenNumbersFromArray', function () {
  let validator;

  beforeEach(function () {
    validator = new NumbersValidator()
  })

  afterEach(function () {
    validator = null
  })

  it('should return true if is Array and every item is a number or is empty', function () {
    expect(!validator.getEvenNumbersFromArray([1,3,4])).to.be.equal(false);
  });

  it('should throw an error if input is not an Array of numbers', function () {
    expect(() => {
      validator.getEvenNumbersFromArray(['4']);
    }).to.throw('[4] is not an array of "Numbers"');
  });

})

describe('isAllNumbers', function () {
  let validator;

  beforeEach(function () {
    validator = new NumbersValidator()
  })

  afterEach(function () {
    validator = null
  })

  it('should return true when input is an array of numbers', function () {
    const inputArray = [1,2,3,4,5];
    expect(validator.isAllNumbers(inputArray)).to.be.equal(true);
  });
  
  it('should throw an error if input is not an array', function () {
    // const inputArray = 1;
    expect(function () {
      validator.isAllNumbers(1);
    }).to.throw('[1] is not an array');
  });
})


describe('isInteger', function () {
  let validator;

  beforeEach(function () {
    validator = new NumbersValidator()
  })

  afterEach(function () {
    validator = null
  })

  it('should return true if a number is an integer', function () {
    expect(validator.isInteger(0)).to.be.equal(true);
  });

  it('should throw an error if input is not an integer', function () {
    expect(function () {
      validator.isInteger("Tom");
    }).to.throw('[Tom] is not a number');
  });

})

  // it('should return false when input is an array of containing data other than numbers', function () {
  //   const inputArray = [1,"2",3,4,"5","John"];
  //   expect(validator.isAllNumbers(inputArray)).to.be.equal(false);
  // });
