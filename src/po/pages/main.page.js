import { BasePage } from "./base.page.js";
import { NavigationBarComponent } from "../components/common/navigationBar.component.js";

export class MainPage extends BasePage {

    constructor() {
        super('https://cloud.google.com');
        this.navigationBar = new NavigationBarComponent();
    }

}

