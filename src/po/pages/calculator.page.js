
import { AddToEstimateComponent, ComputeEngineComponent, ShareEstimateComponent } from "../components/index.js";
// import { ComputeEngineComponent } from "../components/calculator/computeEngine.component.js";

export class CalculatorPage {

    constructor() {
        this.addToEstimate = new AddToEstimateComponent();
        this.computeEngine = new ComputeEngineComponent();
        this.shareEstimate = new ShareEstimateComponent();
    }

    item(param) {
        const selector = {
            linkToComputeEngineButton: '.xhASFc'
        };
        return $(selector[param]);
    }
}