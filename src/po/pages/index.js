import { MainPage } from "./main.page.js";
import { SearchResultsPage } from "./searchResults.page.js";
import { CalculatorPage } from "./calculator.page.js";
import { EstimateSummaryPage } from "./estimateSummary.page.js";

/* 
    @param name { 'mainPage' | 'searchResultsPage' | 'calculatorPage' }
    @returns { MainPage | SearchResultsPage | CalculatorPage }

 */

function pages(name) {
    const items = {
        mainPage: new MainPage(),
        searchResultsPage: new SearchResultsPage(),
        calculatorPage: new CalculatorPage(),
        estimateSummaryPage: new EstimateSummaryPage()
    };
    return items[name];
}

export { CalculatorPage, MainPage, SearchResultsPage, EstimateSummaryPage, pages };