export class EstimateSummaryPage {

    item(param) {
        const selector = {
            numberOfInstances: "//span[text()='4']/ancestor::div[contains(@class, 'EQCBxd')]",
            operatingSystem: "//span[text()='Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)']/ancestor::div[contains(@class, 'EQCBxd')]",
            provisioningModel: "//span[text()='Regular']/ancestor::div[contains(@class, 'EQCBxd')]",
            machineType: "//span[text()='n1-standard-8, vCPUs: 8, RAM: 30 GB']/ancestor::div[contains(@class, 'EQCBxd')]",
            gpuModel: "//span[text()='NVIDIA Tesla V100']/ancestor::div[contains(@class, 'EQCBxd')]",
            numberOfGpus: "//span[text()='1']/ancestor::div[contains(@class, 'EQCBxd')]",
            localSsd: "//span[text()='2x375 GB']/ancestor::div[contains(@class, 'EQCBxd')]",
            commitedUse: "//span[text()='1 year']/ancestor::div[contains(@class, 'EQCBxd')]"
        };
        return $(selector[param]);
    }
}