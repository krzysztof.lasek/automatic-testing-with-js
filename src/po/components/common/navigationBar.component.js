import { BasePage } from "../../pages/base.page.js";

export class NavigationBarComponent extends BasePage {

    get rootEl() {
        return $('.a7K4Uc');
    }

    item(param) {
        const selectors = {
            overview: '[data-navigation-tab-index="0"]',
            solutions: '[data-navigation-tab-index="1"]',
            products: '[data-navigation-tab-index="2"]',
            pricing: '[data-navigation-tab-index="3"]',
            resources: '[data-navigation-tab-index="4"]',
            searchIcon: '.ND91id',
            searchBox: '.mb2a7b'
        };
        return this.rootEl.$(selectors[param]);
    }

}