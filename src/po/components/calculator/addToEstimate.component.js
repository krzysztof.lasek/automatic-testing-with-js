export class AddToEstimateComponent {

    item(param) {
        const selector = {
            accessComputeEngine: "//h2[text()='Compute Engine']/ancestor::div[contains(@class, 'aHij0b-WsjYwc')]"
        };
        return $(selector[param]);
    }

}