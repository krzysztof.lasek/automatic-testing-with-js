export class ComputeEngineComponent {
    
    item(param) {
        const selector = {
            numberOfInstances: '#c11',
            machineType: "//span[text()='Machine type']/ancestor::div[contains(@class, 'O1htCb-H9tDt')]",
            addGpusTrue: "//button[@aria-label='Add GPUs']",
            gpuModel: "//span[text()='GPU Model']/ancestor::div[contains(@class, 'YgByBe')]",
            localSsd: "//span[text()='Local SSD']/ancestor::div[contains(@class, 'YgByBe')]",
            commitedUseDiscount: "//input[@id='1-year']/ancestor::div[contains(@class, 'c0GfYc')]",
            estimatedCost: "label[class='gt0C8e MyvX5d D0aEmf']",
            shareButton: ".FOBRw-vQzf8d",
            openEstimateSummaryButton: ".jl2ntd"
        };
        return $(selector[param]);
    }

}