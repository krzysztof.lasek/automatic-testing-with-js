describe ('Webdriver Task 1', () => {
    beforeEach( async () => {
        // Open https://pastebin.com/ or a similar service in any browser.
        await browser.url('https://pastebin.com');
    })

    it('Webdriver task 1 - automate the script', async () => {
        // Create 'New Paste' with the following attributes:
        await $('.css-47sehv').click();
        // * Code: "Hello from WebDriver"
        await $('#postform-text').addValue("Hello from WebDriver");
        // * Paste Expiration: "10 Minutes"
        await $('#postform-expiration').scrollIntoView({block: 'center', inline: 'center'});
        await $('#select2-postform-expiration-container').click();
        await $('#select2-postform-expiration-container').click({y: 100});
        await browser.pause(2000);
        // * Paste Name / Title: "helloweb"
        await $('#postform-name').addValue("helloweb");
        await browser.pause(2000);
        await $('button.-big').click();
        await browser.pause(2000);
    })
})