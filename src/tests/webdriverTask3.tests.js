
import { pages } from "../po/index.js";

describe ('Webdriver Task 3', () => {
    beforeEach( async () => {
        // 1. Open https://cloud.google.com/.
        await pages('mainPage').open();
    })

    it('Webdriver task 3 - automate the script', async () => {
        await $('.glue-cookie-notification-bar__accept').click();
        // 2. Click on the icon at the top of the portal page and enter "Google Cloud Platform Pricing Calculator" into the search field.
        await pages('mainPage').navigationBar.item('searchIcon').click();
        await pages('mainPage').navigationBar.item('searchBox').addValue("Google Cloud Platform Pricing Calculator");
        
        // 3. Perform the search.
        await browser.keys("Return");
        await browser.pause(2000);
        // 4. Click "Google Cloud Platform Pricing Calculator" in the search results and go to the calculator page.
        await pages('searchResultsPage').item('calculatorLink').click();
   
        // 5. Click COMPUTE ENGINE at the top of the page.
        await pages('calculatorPage').item('linkToComputeEngineButton').click();
        await pages('calculatorPage').addToEstimate.item('accessComputeEngine').click();
     
        // 6. Fill out the form with the data
        // * Datacenter location: Frankfurt (europe-west3) - not available in dropdown
        await pages('calculatorPage').computeEngine.item('numberOfInstances').setValue(4);
        await browser.pause(2000);
        await pages('calculatorPage').computeEngine.item('machineType').scrollIntoView({block: 'center', inline: 'center'});
        await browser.pause(2000);
        await pages('calculatorPage').computeEngine.item('machineType').click();
        await browser.pause(2000);
        await browser.keys("N");
        await browser.pause(2000);
        await browser.keys("Enter");
        await browser.pause(2000);
        await pages('calculatorPage').computeEngine.item('addGpusTrue').scrollIntoView({block: 'center', inline: 'center'});
        await browser.pause(2000);
        await pages('calculatorPage').computeEngine.item('addGpusTrue').click();
        await browser.pause(2000);
        await pages('calculatorPage').computeEngine.item('gpuModel').click();
        await pages('calculatorPage').computeEngine.item('gpuModel').click({y: 150});
        await browser.pause(2000);
        await pages('calculatorPage').computeEngine.item('localSsd').click();
        await pages('calculatorPage').computeEngine.item('localSsd').click({y: -450});
        await browser.pause(2000);
        await pages('calculatorPage').computeEngine.item('commitedUseDiscount').scrollIntoView({block: 'center', inline: 'center'});
        await pages('calculatorPage').computeEngine.item('commitedUseDiscount').click();
        await browser.pause(2000);
        await pages('calculatorPage').computeEngine.item('estimatedCost').scrollIntoView({block: 'center', inline: 'center'});
        await browser.pause(2000);
        // 7. Click 'Add to Estimate'. (add to estimate not clicked - price is calculated automatically. But if the click is required, the appropriate line is commented below.)
        // await $("//span[text()='Add to estimate']/ancestor::div[contains(@class, 'jirROd')]").click();

        // 8. Check the price is calculated in the right section of the calculator. There is a line “Total Estimated Cost: USD ${amount} per 1 month” 
        await expect(pages('calculatorPage').computeEngine.item('estimatedCost')).toBeDisplayed();
        // 9. click "Share" to see Total estimated cost
        await pages('calculatorPage').computeEngine.item('shareButton').click();
        // 10. click "Open estimate summary" to see Cost Estimate Summary, will be opened in separate tab browser.
        await pages('calculatorPage').shareEstimate.item('openEstimateSummary').click();
        // 11. verify that the 'Cost Estimate Summary' matches with filled values in Step 6.
        await browser.switchWindow('Google Cloud Estimate Summary');
        await expect(pages('estimateSummaryPage').item('numberOfInstances')).toHaveTextContaining('4');
        await expect(pages('estimateSummaryPage').item('operatingSystem')).toHaveTextContaining('Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)');
        await expect(pages('estimateSummaryPage').item('provisioningModel')).toHaveTextContaining('Regular');
        await expect(pages('estimateSummaryPage').item('machineType')).toHaveTextContaining('n1-standard-8, vCPUs: 8, RAM: 30 GB');
        await expect(pages('estimateSummaryPage').item('gpuModel')).toHaveTextContaining('NVIDIA Tesla V100');
        await expect(pages('estimateSummaryPage').item('numberOfGpus')).toHaveTextContaining('1');
        await expect(pages('estimateSummaryPage').item('localSsd')).toHaveTextContaining('2x375 GB');
        await expect(pages('estimateSummaryPage').item('commitedUse')).toHaveTextContaining('1 year');
    })
})