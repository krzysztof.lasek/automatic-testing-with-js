
describe ('Webdriver Task 2', () => {
    beforeEach( async () => {
        // 1. Open https://pastebin.com/ or a similar service in any browser.
        await browser.url('https://pastebin.com');
    })

    it('Webdriver task 2 - automate the script', async () => {
        // 2. Create 'New Paste' with the following attributes:
        await $('.css-47sehv').click();
        await $('#postform-text').addValue('git config --global user.name  "New Sheriff in Town"');
        await browser.keys("Enter");
        await $('#postform-text').addValue('git reset $(git commit-tree HEAD^{tree} -m "Legacy code")');
        await browser.keys("Enter");
        await $('#postform-text').addValue('git push origin master --force');
        await browser.pause(2000);
        // * Syntax Highlighting: "Bash"
        await $('#postform-expiration').scrollIntoView({block: 'center', inline: 'center'});
        await $('#select2-postform-format-container').click();
        await browser.keys("B");
        await browser.keys("Enter");
        await browser.pause(2000);
        // * Paste Expiration: "10 Minutes"
        await $('#select2-postform-expiration-container').click();
        await $('#select2-postform-expiration-container').click({y: 100});
        await browser.pause(2000);
        // * Paste Name / Title: "how to gain dominance among developers"
        await $('#postform-name').addValue("how to gain dominance among developers");
        await browser.pause(2000);
        // 3. Save 'paste' and check the following:
        await $('button.-big').click();
        await browser.pause(5000);
        // * Browser page title matches 'Paste Name / Title'
        await expect(browser).toHaveTitle('how to gain dominance among developers - Pastebin.com');
        // * Syntax is suspended for bash
        await expect($('ol[class="bash"]')).toBeDisplayed();
        // * Check that the code matches the one from paragraph 2
        await $("//a[text()='raw']/ancestor::div[contains(@class, 'right')]").click();
        expect($('//body//pre')).toHaveValue('git config --global user.name  "New Sheriff in Town" git reset $(git commit-tree HEAD^{tree} -m "Legacy code") git push origin master --force ');
    })
})